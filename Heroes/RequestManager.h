//
//  RequestManager.h
//  Heroes
//
//  Created by Julio Serrano on 11/19/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ResponseFromServicesDelegate <NSObject>
    -(void)responseFromService:(NSMutableDictionary *)response;
@end

@interface RequestManager : NSObject<NSURLSessionDelegate>{
    /*!
     * @brief Flag to distinguish if is the first time that request a service.
     */
    BOOL            isFirstRequest;
    /*!
     * @brief The Global session, used in all WS requests.
     */
    NSURLSession    *session;
    
}

/*!
 * @brief RM delegate, basically the object that makes the request, and waits for the response.
 */
@property (nonatomic, weak)     id<ResponseFromServicesDelegate>    delegate;

+ (instancetype)sharedInstance;

- (void) configureSession;

- (void) sendSimpleRequest:(RequestType)requestType toMethod:(NSString*)httpMethod;

@end

NS_ASSUME_NONNULL_END
