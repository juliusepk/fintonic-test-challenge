//
//  SceneDelegate.h
//  Heroes
//
//  Created by Julio Serrano on 11/19/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

