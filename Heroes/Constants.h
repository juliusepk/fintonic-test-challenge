//
//  Constants.h
//  Heroes
//
//  Created by Julio Serrano on 11/19/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

typedef enum {
    // super heroes list
    kRequestSuperHeroesList
} RequestType;

#endif /* Constants_h */
