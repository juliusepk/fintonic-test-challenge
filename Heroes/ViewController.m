//
//  ViewController.m
//  Heroes
//
//  Created by Julio Serrano on 11/19/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

#import "ViewController.h"
#import "RequestManager.h"

@interface ViewController ()<ResponseFromServicesDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestHeroesList];
}

- (void)requestHeroesList{
    [RequestManager sharedInstance].delegate = self;
    [[RequestManager sharedInstance] sendSimpleRequest:kRequestSuperHeroesList toMethod:@"GET"];
}



- (void)responseFromService:(nonnull NSMutableDictionary *)response {
    NSLog(@"response: %@",response);
}


@end
