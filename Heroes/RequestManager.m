//
//  RequestManager.m
//  Heroes
//
//  Created by Julio Serrano on 11/19/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

#import "RequestManager.h"
#import "Reachability.h"
//#import "Constants.h"

#define GET_SUPERHEROES_LIST @"https://api.myjson.com/bins/bvyob"

@implementation RequestManager

static RequestManager *manager = nil;

+(instancetype)sharedInstance{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [[RequestManager alloc] init];
    });
    return manager;
}

-(instancetype)init
{
    if (self = [super init])
    {
        isFirstRequest = true;
        [self configureSession];
    }
    return self;
}

- (void) configureSession {
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultSessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringCacheData;
    defaultSessionConfiguration.HTTPMaximumConnectionsPerHost = 1;
    defaultSessionConfiguration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyNever;
    defaultSessionConfiguration.HTTPShouldSetCookies = false;
    defaultSessionConfiguration.timeoutIntervalForRequest = 20;
    defaultSessionConfiguration.timeoutIntervalForResource = 20;
    session = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:nil];
}

- (void) sendSimpleRequest:(RequestType)requestType toMethod:(NSString*)httpMethod{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {     // no internet
        [self.delegate responseFromService:[@{@"net_error":[NSString stringWithFormat:@"400"],
                                              @"method_code":[NSNumber numberWithInt:requestType]} mutableCopy] ]; // ERROR
    } else {
        NSMutableURLRequest *request = [self getBaseRequestToRequest:requestType andHTTPMethod:httpMethod];
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            @try {
                if (error != nil){
                    [self.delegate responseFromService:[@{@"net_error":[NSString stringWithFormat:@"%d",(int)error.code],
                    @"method_code":[NSNumber numberWithInt:requestType]} mutableCopy] ];
                } else {
//                    NSLog(@"Post: %@", response);
//                    NSDictionary* datax = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//                    NSLog(@"Postd: %@", datax);
                    
                    if (requestType == kRequestSuperHeroesList){
                        NSLog(@"STR: '%@'",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
                    }
                    
                    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                    if (response == NULL) {
                        NSLog(@"response is null");
                        response = @{};
                    }
                    
                    [self.delegate responseFromService:[@{@"response":response, @"method_code":[NSNumber numberWithInt:requestType]} mutableCopy]];
//                    else if (self->currentRequest == kRequestJATOData){
//                        DLog(@"JatoInfo success");
//                    } else if (self->currentRequest == kRequestJATOGetAdvantages) {
//                        DLog(@"JatoAdventages success");
//                    } else {
//                        DLog(@"STR: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
//                    }
                    
//                    if ( (self->currentRequest == kRequestLogin && [(NSHTTPURLResponse*)response statusCode] == 400) ){
//
//                        NSString *error = [NSString stringWithFormat:@"%@",[[datax objectForKey:@"error"] uppercaseString]];
//                        if([error isEqualToString:INVALID_GRANT] || [error isEqualToString:UNSOPORTED_GT]){
//                            [self showAlertFailRequest];
//                        }else if([error isEqualToString:INVALID_CREDENTIALS]){
//                            [self showAlertInvalidCredentials:[datax objectForKey:@"error_description"]];
//                        }else{
//                            [self showAlertFailRequest];
//                        }
//                    }
                    
//                    if (self->currentRequest == kRequestGetMassiveCar || self->currentRequest == kRequestGetPlanCar)
//                    {
//                        NSArray* response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//                        [self.delegate responseFromService:[@{@"response":response, @"method_code":[NSNumber numberWithInt:self->currentRequest]} mutableCopy]];
//                    }
//                    else if ([self isSimpleResponse:requestType])
//                    {
//                        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//                        if (response == NULL) {
//                            NSLog(@"response is null");
//                            response = @{};
//                        }
//
//                        [self.delegate responseFromService:[@{@"response":response, @"method_code":[NSNumber numberWithInt:self->currentRequest]} mutableCopy]];
//                    }
//                    else if (self->currentRequest == kRequestJATOAddVehicle || self->currentRequest == kRequestJATOAddBenchmarkVehicle)
//                    {
//                        [self.delegate responseFromService:[@{@"response":[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding], @"method_code":[NSNumber numberWithInt:self->currentRequest]} mutableCopy]];
//                    }
//                    else
//                    {
//                        if(self->currentRequest == kRequestCharacteristicsData)
//                        {
//                            NSArray* respuesta = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//                            NSMutableDictionary *response = [[NSMutableDictionary alloc] initWithCapacity:respuesta.count];
//                            for (NSDictionary *parametros in respuesta)
//                            {
//                                [response setValue:[parametros objectForKey:@"id"] forKey:[parametros objectForKey:@"name"]];
//                            }
//                            [self.delegate responseFromService:[response mutableCopy]];
//                        }
//                        else if (self->currentRequest == kRequestSendCharacteristicsData)
//                        {
//                            [DataBaseManager updateCharacteristicslStatsStatus];
//                        }
//                        else
//                        {
//                            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//
//                            if ([responseDict objectForKey:@"Message"] != nil)
//                            {
//                                [self.delegate responseFromService:[@{@"niss_net_error":@"error", @"method_code":[NSNumber numberWithInt:self->currentRequest]} mutableCopy] ];
//                            }
//                            else
//                            {
//                                /*if (self->currentRequest == KSaveCheckList ) {
//                                    responseDict = [self proccessInfoRequest:data andResponse:(NSHTTPURLResponse*)response];
//                                }*/
//                                [self.delegate responseFromService:[responseDict mutableCopy]];
//
//                            }
//                        }
//                    }
                }
            } @catch (NSException *ex) {
                [self.delegate responseFromService:[@{@"net_error":[NSString stringWithFormat:@"-100"],
                @"method_code":[NSNumber numberWithInt:requestType]} mutableCopy] ];
            }
        }];
        
        [postDataTask resume];
    }
}

- (NSMutableURLRequest*) getBaseRequestToRequest:(RequestType)requestType andHTTPMethod:(NSString*)httpMethod{
    NSMutableURLRequest *request    = [[NSMutableURLRequest alloc] init];
    NSString *str_url               = [self getURLRequest:requestType];
    request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str_url]];
    [request setHTTPMethod:httpMethod];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    return request;
}

-(NSString *)getURLRequest:(RequestType)requestType{
    switch (requestType) {
        case kRequestSuperHeroesList:
            return GET_SUPERHEROES_LIST;
        default:
            return @"";
    }
}

@end
