//
//  AppDelegate.h
//  Heroes
//
//  Created by Julio Serrano on 11/19/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

